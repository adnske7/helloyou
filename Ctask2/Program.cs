﻿using System;

namespace Ctask2
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Please enter your username: ");
			string UserName = Console.ReadLine();
			Console.WriteLine("Hello "+ UserName + " your username is " + 
				UserName.Length+" characters long and starts with the letter " + UserName[0]);
		}
	}
}
