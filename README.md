#HelloYou
A small program that takes the users name as an text input and returns a greeting to the user and displays how many letters are in the name and what the first letter is.


## Getting Started
Open the folder in Visual studio and run the program.cs file.

## Author

* **Ådne Skeie** [adnske7](https://gitlab.com/adnske7)

